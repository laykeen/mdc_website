# Museo del Computer Website and Management

## Development

### Utilities

- [https://github.com/tachyons-css/tachyons](https://github.com/tachyons-css/tachyons) tachyons official repo
- [https://tachyons-tldr.now.sh/#/classes](https://tachyons-tldr.now.sh/#/classes) for tachyons references

### Tutorials

- [https://github.com/dwyl/learn-tachyons](https://github.com/dwyl/learn-tachyons) fast learn tachyons

### Stuff that can help A LOT

- [stackoverflow: stretch-svg-background-image](https://stackoverflow.com/questions/39418463/stretch-svg-background-image)
